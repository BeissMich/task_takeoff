import React, {useEffect, useState} from 'react';
import {useNavigate, Route, Routes} from "react-router-dom";
import {Box, Grid} from "@mui/material";
import {useDispatch, useSelector} from "react-redux";
import {Provider} from "react-redux";
import store, {RootState} from "./redux/redux-store";
import Login from "./pages/Login/Login";
import UsersList from "./pages/UsersList/UsersList";
import {setUsers} from "./redux/reducers/users_reducer";

const generalBox = {
    height: '100vh',
    width: '100vw',
    display: 'flex',
}

function App() {
    const dispatch = useDispatch()
    const loginData = useSelector((state: RootState) => state.loginPage.loginData);

    let navigate = useNavigate();

    const login = true;


    useEffect(() => {
        if (!login) {
            navigate("login");
        } else {
            navigate("/");
        }
    }, [login, navigate]);

    useEffect(() => {
        fetch(
            `http://www.filltext.com/?rows=${500}&id={number|1000}&firstName={firstName}&lastName={lastName}&email={email}&phone={phone|(xxx)xxx-xx-xx}&address={addressObject}&description={lorem|32}`
        )
            .then((res) => res.json())
            .then((data) => {
                // dispatch(getUsers(data));
                dispatch(setUsers(data))
                console.log("data!!!", data)
            })
            .catch((error) => alert(error));
    }, []);


    return (
        <Box sx={generalBox}>
            <Routes>
                <Route path="/" element={<UsersList/>}/>
                <Route path="login" element={<Login/>}/>
            </Routes>
        </Box>
    );
}


const MainApp = () => {
    return (
        <Provider store={store}>
            <App/>
        </Provider>
    );
};
export default MainApp;
