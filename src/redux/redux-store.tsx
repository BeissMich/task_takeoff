import {createStore, combineReducers} from "redux";
import reducerLogin from "./reducers/login_reducer";
import reducerUsers from "./reducers/users_reducer";

let reducers = combineReducers({
    loginPage: reducerLogin,
    usersReducer: reducerUsers,
});


const store = createStore(
    reducers
);
// @ts-ignore
window.__store__ = store;

export type RootState = ReturnType<typeof store.getState>

export type AppDispatch = typeof store.dispatch

export default store;