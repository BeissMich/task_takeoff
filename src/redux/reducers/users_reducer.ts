import User from "../../Types";
import { ActionTypes } from "../ActionTypes";

interface initialStateProp {
    users: Array<User>
}

let initialState: initialStateProp = {
    users: [],
};

const reducerUsers = (state = initialState, action: any) => {
    switch (action.type) {
        case ActionTypes.SET_USERS:
            return {
                ...state,
                users: action.users,
            };
        case ActionTypes.EDIT_USER:
            const newArrUsers:Array<User> = state.users.map((user: User) => {
                if (user.id === action.editUser.id) {
                    return action.editUser
                } else {
                    return user
                }
            });
            return {
                ...state,
                users: newArrUsers,
            };

        default:
            return state;
    }
}
    ;


export const setUsers = (users: any) => {
    return {
        type: ActionTypes.SET_USERS,
        users,
    };
};
export const editUser = (editUser: any) => {
    return {
        type: ActionTypes.EDIT_USER,
        editUser,
    };
};


export default reducerUsers;