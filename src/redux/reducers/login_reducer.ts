import {ActionTypes} from "../ActionTypes";


let initialState = {
    loginData: {},
};

const reducerLogin = (state = initialState, action: any) => {
        switch (action.type) {
            case ActionTypes.LOGIN:
                return {
                    ...state,
                    loginData: action.data,
                    authToken: action.data.result.auth_token,
                    role: action.data.result.role,
                };

            default:
                return state;
        }
    }
;


export const loginAuth = (data: any) => {
    return {
        type: ActionTypes.LOGIN,
        data,
    };
};


export default reducerLogin;