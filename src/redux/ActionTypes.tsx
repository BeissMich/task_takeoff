export enum ActionTypes {
    LOGIN = "LOGIN",
    SET_USERS = "SET_USERS",
    EDIT_USER = 'EDIT_USER'
}