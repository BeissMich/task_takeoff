export default interface User {
    id: string | number,
    lastName: string;
    firstName: string,
    email: string,
    phone: string
}