import React, { useState } from "react";
import {
    Box,
    Button,
    Card,
    CardHeader, dividerClasses,
    FormControl, FormControlLabel,
    Grid, IconButton,
    Input,
    InputAdornment,
    InputLabel, Modal, Paper,
    TextField,
    Typography
} from "@mui/material";
import SearchIcon from '@mui/icons-material/Search';
import AddCircleOutlineIcon from '@mui/icons-material/AddCircleOutline';
import EditIcon from '@mui/icons-material/Edit';
import { DataGrid, GridColDef, GridValueGetterParams } from '@mui/x-data-grid';
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "../../redux/redux-store";
import { editUser } from "../../redux/reducers/users_reducer";
import User from "../../Types";

const style = {
    position: 'absolute' as 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 400,
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    p: 4,
};


const UsersList = () => {

    const dispatch = useDispatch();

    const [open, setOpen] = React.useState(false);
    const handleClose = () => setOpen(false);
    const [selectedUser, setSelectedUser] = useState<any | undefined>();
    const [newUser, setNewUser] = useState<User | undefined>();

    const usersRows = useSelector((state: RootState) => state.usersReducer.users);
    console.log("usersRows", usersRows)

    const saveChanges = (e: any) => {
        e.preventDefault();
        dispatch(editUser(selectedUser));
    }

    interface EditProps {
        user: User,
    }
    const MatEdit: React.FC<EditProps> = ({ user }: EditProps) => {
        const handleEditClick = () => {
            setOpen(true);
            setSelectedUser(user)
        }
        return <FormControlLabel label={''}
            control={
                <IconButton color="secondary" aria-label="add an alarm" onClick={handleEditClick}>
                    <EditIcon style={{ color: 'rgb(6, 175, 236)' }} />
                </IconButton>
            }
        />
    };

    const columns: GridColDef[] = [
        { field: 'id', headerName: 'ID', type: 'string', width: 70, editable: true },
        { field: 'firstName', headerName: 'First name', type: 'string', width: 130, editable: true },
        { field: 'lastName', headerName: 'Last name', type: 'string', width: 130, editable: true },
        {
            field: 'email',
            headerName: 'Email',
            type: 'string',
            width: 150,
            editable: true
        }, {
            field: 'phone',
            headerName: 'Phone',
            type: 'string',
            width: 130,
            editable: true
        },
        {
            field: 'edit',
            headerName: 'Edit',
            type: 'any',
            width: 90,
            renderCell: (params) => {
                console.log("params", params.row)
                return (
                    <div className="d-flex justify-content-between align-items-center" style={{ cursor: "pointer" }}>
                        <MatEdit user={params.row} />
                    </div>
                );
            }
        },
    ];


    return <Grid container direction="row"
        justifyContent="space-around" alignItems="flex-start">
        <Grid container justifyContent="flex-start"
            direction="column" rowSpacing={3} style={{ width: "80%" }}>
            <Grid item>
                <Card style={{ padding: '20px' }}>
                    <Grid>
                        <CardHeader title={<Typography>Lalalal</Typography>} />
                    </Grid>
                </Card>
            </Grid>
            <Grid item>
                <Card style={{
                    padding: '20px',
                    height: '80vh',
                }}>
                    <Grid container
                        direction="column"
                        justifyContent="space-around"
                        alignItems="center" rowSpacing={1}>
                        <Grid item>
                            <Grid container
                                justifyContent="space-around"
                                alignItems="center">
                                <FormControl variant="standard" >
                                    <Input
                                        endAdornment={
                                            <InputAdornment position="end">
                                                <IconButton
                                                    aria-label="toggle password visibility"
                                                >
                                                    <SearchIcon />
                                                </IconButton>
                                            </InputAdornment>
                                        }
                                    />
                                </FormControl>
                                <AddCircleOutlineIcon style={{ cursor: 'pointer', color: 'rgb(6, 175, 236)' }} />
                            </Grid>
                        </Grid>
                        <Grid item style={{ width: '67%' }}>
                            <Grid style={{ height: 300, width: '100%' }}>
                                <DataGrid
                                    rows={usersRows}
                                    columns={columns}
                                    experimentalFeatures={{ newEditingApi: true }}
                                />
                            </Grid>
                        </Grid>
                    </Grid>
                </Card>
            </Grid>
        </Grid>
        <Modal
            open={open}
            onClose={handleClose}
            aria-labelledby="modal-modal-title"
            aria-describedby="modal-modal-description"
        >
            <Box sx={style}>
                <Typography id="modal-modal-title" variant="h6" component="h2">
                    Edit mode
                </Typography>
                <FormControl>
                    <Grid container rowSpacing={2} component="form" onSubmit={saveChanges}>
                        <Grid xs={12} item>
                            <TextField
                                required
                                id="firstName"
                                label="First Name"
                                name='firstName'
                                onChange={(e: any) => {
                                    const firstName: string = e.target.value;
                                    setSelectedUser({ ...selectedUser, firstName })
                                }}
                                value={selectedUser?.firstName}
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                required
                                id="lastName"
                                label="Last Name"
                                name='lastName'
                                onChange={(e: any) => {
                                    const lastName = e.target.value;
                                    setSelectedUser({ ...selectedUser, lastName })
                                }}
                                defaultValue={selectedUser?.lastName}
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                required
                                id="email"
                                label="Email"
                                name='email'
                                onChange={(e: any) => {
                                    const email = e.target.value;
                                    setSelectedUser({ ...selectedUser, email })
                                }}
                                defaultValue={selectedUser?.email}
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                required
                                id="phone"
                                label="Phone"
                                name='phone'
                                onChange={(e: any) => {
                                    const email = e.target.value;
                                    setSelectedUser({ ...selectedUser, email })
                                }}
                                defaultValue={selectedUser?.phone}
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <Button variant="contained" type='submit'>Save</Button>
                        </Grid>
                    </Grid>
                </FormControl>
            </Box>
        </Modal>
        <Modal
            open={open}
            onClose={handleClose}
            aria-labelledby="modal-modal-title"
            aria-describedby="modal-modal-description"
        >
            <Box sx={style}>
                <Typography id="modal-modal-title" variant="h6" component="h2">
                    Create new user
                </Typography>
                <FormControl>
                    <Grid container rowSpacing={2} component="form" onSubmit={saveChanges}>
                        <Grid xs={12} item>
                            <TextField
                                required
                                id="firstName"
                                label="First Name"
                                name='firstName'
                                onChange={(e: any) => {
                                    const firstName: string = e.target.value;
                                    setSelectedUser({ ...newUser, firstName })
                                }}
                                value={newUser?.firstName}
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                required
                                id="lastName"
                                label="Last Name"
                                name='lastName'
                                onChange={(e: any) => {
                                    const lastName = e.target.value;
                                    setSelectedUser({ ...newUser, lastName })
                                }}
                                defaultValue={newUser?.lastName}
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                required
                                id="email"
                                label="Email"
                                name='email'
                                onChange={(e: any) => {
                                    const email = e.target.value;
                                    setSelectedUser({ ...newUser, email })
                                }}
                                defaultValue={newUser?.email}
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                required
                                id="phone"
                                label="Phone"
                                name='phone'
                                onChange={(e: any) => {
                                    const email = e.target.value;
                                    setSelectedUser({ ...newUser, email })
                                }}
                                defaultValue={newUser?.phone}
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <Button variant="contained" type='submit'>Save</Button>
                        </Grid>
                    </Grid>
                </FormControl>
            </Box>
        </Modal>
    </Grid>
}

export default UsersList;