import React from 'react';
import {Button, Card, FormControl, Grid, TextField, Typography} from "@mui/material";


const Login = () => {

    return <Grid container spacing={1} direction="row"
                 justifyContent="space-around" alignItems="center">
        <Card style={{padding: '20px'}}>
            <Grid container
                  direction="column"
                  justifyContent="space-around"
                  alignItems="center" rowSpacing={1}>
                <Grid item>
                    <Typography variant="h6" component="h5">
                        Login
                    </Typography>
                </Grid>
                <Grid item>
                    <FormControl>
                        <Grid direction="column" container rowSpacing={2}>
                            <Grid item>
                                <TextField
                                    id="outlined-password-input"
                                    label="Login"
                                />
                            </Grid>

                            <Grid item>
                                <TextField
                                    id="outlined-password-input"
                                    label="Password"
                                    type="password"
                                    autoComplete="current-password"
                                />
                            </Grid>
                            <Grid item>
                                <Button variant="contained">Log In</Button>
                            </Grid>
                        </Grid>
                    </FormControl>
                </Grid>
            </Grid>
        </Card>
    </Grid>
}

export default Login;